import * as AWS from 'aws-sdk'
import {JsonProfile} from './profileInterfaces'

export class ProfileController {
  constructor() {
    AWS.config.update({
      region: "us-west-2",
      credentials: {
        accessKeyId: 'AKIAJJE3LQBPLJAT5LFA',
        secretAccessKey: '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'
      }
    })
  }

  public getProfileFromServer(guid: string): Promise<JsonProfile> {
    const docClient = new AWS.DynamoDB.DocumentClient()

    const params: AWS.DynamoDB.DocumentClient.GetItemInput = {
      TableName: 'BetaProfiles',
      Key: {
        guid: guid
      }
    }

    return new Promise<JsonProfile>((resolve, reject) => {
      docClient.get(params, (err, data) => {
        if (err) {
          reject("Unable to get:" + guid + ' Error JSON:' + JSON.stringify(err, null, 2))
        } else {
          resolve(data.Item as JsonProfile)
        }
      })
    })
  }
}
