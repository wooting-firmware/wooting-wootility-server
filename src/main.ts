import * as express from 'express'
import * as bodyParser from 'body-parser'
import { ProfileController } from './profile-server'

let profileController = new ProfileController()

let app = express() // define our app using express

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

let port = process.env.PORT || 8080;        // set our port

let router = express.Router();              // get an instance of the express Router

router.route('/bears').post(function(req, res) {
  let name = req.body.name;  // set the bears name (comes from the request)

  if (name == 'bear') {
    res.json({ message: 'Bear created!' })
  }
})

router.route('/profiles/:profile_id')
  .get((req, res) => {
    profileController.getProfileFromServer(req.params.profile_id)
    .then(data => res.json(data))
    .catch(err => res.status(404).send(err))
  })

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router)

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port)
