export interface JsonProfile {
  guid: string
  name: string
  desc: string
  analog: JsonAnalDigitProfile
  rgb: IpcColorProfile
  digital: JsonAnalDigitProfile
}

interface JsonAnalDigitProfile {
  actPoint: number
  enableDigital?: boolean
  mapping?: IXboxMappingArrayIndex
  analogCurve?: Array<number>
}

interface IpcColorProfile {
  kbdArray: Array<Array<RgbColor>>
  brightness: number
}

interface RgbColor {
  red: number
  green: number,
  blue: number
}

interface IXboxMappingArrayIndex {
  // Analog buttons
  leftJoystickUp: KbdIndex
  leftJoystickDown: KbdIndex
  leftJoystickLeft: KbdIndex
  leftJoystickRight: KbdIndex
  rightJoystickUp: KbdIndex
  rightJoystickDown: KbdIndex
  rightJoystickLeft: KbdIndex
  rightJoystickRight: KbdIndex
  leftTrigger: KbdIndex
  rightTrigger: KbdIndex
  // Digital buttons
  buttonA: KbdIndex
  buttonB: KbdIndex
  buttonX: KbdIndex
  buttonY: KbdIndex
  arrowUp: KbdIndex
  arrowDown: KbdIndex
  arrowLeft: KbdIndex
  arrowRight: KbdIndex
  leftBumper: KbdIndex
  rightBumper: KbdIndex
  start: KbdIndex
  select: KbdIndex
  leftJoystickPress: KbdIndex
  rightJoystickPress: KbdIndex
  xboxButton: KbdIndex
}

interface KbdIndex {
  rowNr: number
  colNr: number
  id?: string
}
